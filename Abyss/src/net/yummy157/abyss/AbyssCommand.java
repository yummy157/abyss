package net.yummy157.abyss;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class AbyssCommand implements CommandExecutor
{
	
	public AbyssPlugin plugin;
	
	public AbyssCommand(AbyssPlugin plugin)
	{
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(!(sender instanceof Player))
		{
			sender.sendMessage(Util.fixString("&cMusisz byc graczem online aby uzyc tej komendy!"));
		}
		Player player = (Player) sender;
		if(args.length == 0)
		{
			player.openInventory(plugin.getAbyssManager().inventory);
			return false;
		}
		if(args[0].equalsIgnoreCase("debug"))
		{
			for(int i = 0; i<= 100; i++)
			player.getLocation().getWorld().dropItemNaturally(player.getLocation().add(2,2,2), new ItemStack(Material.DIAMOND_BLOCK, 64));
		}
		else
		{
			plugin.getAbyssManager().open();
		}
		return false;
	}

}
