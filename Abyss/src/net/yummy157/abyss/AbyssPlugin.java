package net.yummy157.abyss;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class AbyssPlugin extends JavaPlugin
{
	private AbyssManager abyssManager;
	private int period;
	private int warn;

	public void onEnable()
	{
		abyssManager = new AbyssManager(this);
		loadConfig();
		getCommand("otchlan").setExecutor(new AbyssCommand(this));
		period = getConfig().getInt("period");
		warn = getConfig().getInt("warn");
		startTask();
		Bukkit.getLogger().info("[Abyss] Zaladowane! Author; yummy157");
	}

	public void onDisable()
	{
		abyssManager = null;
		Bukkit.getLogger().info("[Abyss] Plugin wylaczony!");

	}

	public void loadConfig()
	{
		getConfig().addDefault("period", 600);
		getConfig().addDefault("warn", 30);
		getConfig().addDefault("cleanOld", true);
		getConfig().addDefault("messages.warn", "&cOtchlan zostanie otwarta za <WARN> sekund!");
		getConfig().addDefault("messages.open", "&2Otchlan zostala otwarta i pochlonela &e<NUM> &2przedmiotów!");
		getConfig().options().copyDefaults(true);
		saveConfig();
	}

	public AbyssManager getAbyssManager()
	{
		return abyssManager;
	}

	public void startTask()
	{
		Bukkit.getScheduler().runTaskTimerAsynchronously(this, () -> {
			abyssManager.fillInventory();
			period--;
			if (period == warn)
			{
				Bukkit.broadcastMessage(Util.fixString(getConfig().getString("messages.warn")).replaceAll("<WARN>",
						Integer.toString(warn)));

			} else if (period == 0)
			{
				Bukkit.broadcastMessage(Util.fixString(getConfig().getString("messages.open")).replaceAll("<NUM>",
						Integer.toString(abyssManager.open())));
				period = getConfig().getInt("period");
			}
		}, 20, 20);
	}

}
