package net.yummy157.abyss;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class AbyssManager
{
	public final String title = "Otchlan";
	public Inventory inventory = Bukkit.createInventory(null, 54, title);
	private List<ItemStack> items = new ArrayList<ItemStack>();
	private AbyssPlugin plugin;
	
	public AbyssManager(AbyssPlugin plugin)
	{
		this.plugin = plugin;
	}
	
	public int open()
	{
		items.clear();
		if(plugin.getConfig().getBoolean("cleanOld") == true)
		{
			inventory.clear();
		}
		for(World world : Bukkit.getWorlds())
		{
			for(Entity entity : world.getEntities())
			{
				if(entity instanceof Item)
				{
					Item item = (Item) entity;
					items.add(item.getItemStack().clone());
					item.remove();
				} 
			}
		}
		int size = 0;
		for(ItemStack is : items)
		{
			size += is.getAmount();
		}
		fillInventory();
		return size;
	}
	
	public void fillInventory()
	{
		while(inventory.firstEmpty() != -1 && items.size() != 0)
		{
			inventory.addItem(items.get(0));
			items.remove(0);
		}
	}


}
